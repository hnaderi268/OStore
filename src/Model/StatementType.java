package Model;

public enum StatementType {
	NotCompiled,
	Exp,
	Set,
	If,
	Concat,
	Echo,
	Goto,
}
