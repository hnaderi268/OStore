package Model;

import java.util.ArrayList;
import java.util.Stack;

public class Process {
	private ArrayList<Statement> textCode;
	private int programCounter = 0;
	private Stack<String> returnStack = null;
	

	public Process(){
		textCode = new ArrayList<Statement>();
		programCounter = 0;
	}
	
	public int getSize(){
		return textCode.size();
	}
	public void flush(){
		programCounter = 0;
		textCode = new ArrayList<Statement>();
		returnStack = null;
	}
	public void replaceStatement(ArrayList<Statement> r){
		int i = programCounter;
		textCode.remove(i);
		if(r == null)
			System.out.println("wtf");
		
		for (Statement statement : r) {
			textCode.add(i, statement);
			i++;
		}
	}
	public void pushStatement(String s){
		Statement statement = new Statement();
		statement.setStatement(s);
		textCode.add(statement);
	}
	public boolean hasNext(){
		if(programCounter == textCode.size())
			return false;
		return true;
	}
	public Statement getNext(){
		return textCode.get(programCounter);
	}
	public int getProgramCounter() {
		return programCounter;
	}
	public void setProgramCounter(int programCounter) {
		this.programCounter = programCounter;
	}
}