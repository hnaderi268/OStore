package Model;

public class Statement {
	private String statement;
	private StatementType type = StatementType.NotCompiled;
	private int lablel;
	private String returnValue;
	
	public StatementType getType() {
		return type;
	}
	public void setType(StatementType type) {
		this.type = type;
	}
	public int getLablel() {
		return lablel;
	}
	public void setLablel(int lablel) {
		this.lablel = lablel;
	}
	public String getReturnValue() {
		return returnValue;
	}
	public void setReturnValue(String returnValue) {
		this.returnValue = returnValue;
	}
	public boolean isAssembly(){
		if(type != StatementType.NotCompiled)
			return true;
		return false;
	}
	public String getStatement() {
		return statement;
	}
	public void setStatement(String statement) {
		this.statement = statement;
	}
}
