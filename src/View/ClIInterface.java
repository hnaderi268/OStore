package View;

import java.util.Scanner;

import Interpreter.Interpreter;
import Model.Process;

public class ClIInterface {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Process p = new Process();
		Interpreter interpreter = new Interpreter();

		while (true) {
			String s = sc.nextLine();
			p.pushStatement(s);
			interpreter.resetInterpreer();
			interpreter.setRunningProcess(p);
			try {
				interpreter.startRunning();
			} catch (Exception e) {
				System.out.println("input command is invalid.");
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}
}
// if (1==2)   {echo(sv)}