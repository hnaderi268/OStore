package Interpreter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import Model.Process;
import Model.Statement;

public class Assembler {
	private ArgomanSeparator separator;
	HashMap<String, Integer> numberMap = new HashMap<String, Integer>();
	HashMap<String, String> stringMap = new HashMap<String, String>();

	public void assembleAndRun(Process p, Statement statement, Stack<String> returnStack) throws Exception {

		ArgomanSeparator separator = new ArgomanSeparator();
		ArrayList<String> argoms = new ArrayList();

		switch (statement.getType()) {

		case Echo:
			String input = returnStack.pop();
			// check if input is just a string in quote marks
			if (input.matches("^\".*\"$"))
				System.out.println(input);
			// check if input is a number
			else if(input.matches("^\\d*$"))
				System.out.println(input);
			// check if input is a variable
			else if (input.matches("^\\w*$"))
				if (numberMap.containsKey(input))
					System.out.println(numberMap.get(input).intValue());
				else if (stringMap.containsKey(input))
					System.out.println(stringMap.get(input).toString());
			// check input is a expression
			else {
				// give it to expression evaluator
				System.out.println("Nothing is going to happen");
			}
			p.setProgramCounter(p.getProgramCounter() + 1);
			break;
		case Set:
			//seperate argomans
			argoms = separator.separate(returnStack.pop(), ',');
			//check if value is a String
			if ((argoms.get(1)).matches("^\".*\"$"))
				stringMap.put(argoms.get(0), argoms.get(1));
			//check if value is an Integer
			else if (((String) argoms.get(1)).matches("^\\d*$")) 
				numberMap.put(argoms.get(0), Integer.parseInt(argoms.get(1)));
			//value is not valid
			else
				throw new CMDEXEP("Value is not valid.");
			p.setProgramCounter(p.getProgramCounter() + 1);
			break;
		case If:
			System.out.println(returnStack.pop());
			p.setProgramCounter(p.getProgramCounter() + 1);
			break;
		case Exp:
			returnStack.push(statement.getStatement());
			p.setProgramCounter(p.getProgramCounter() + 1);
			break;
		}
	}
}

class CMDEXEP extends Exception {

	CMDEXEP(String message) {
		super(message);
	}
}