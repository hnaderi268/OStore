package Interpreter;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Model.Statement;
import Model.StatementType;

public class Compiler {

	public Compiler() {

	}

	public ArrayList<Statement> compile(Statement statement) {
		Pattern echoPattern = Pattern.compile("^echo\\s*\\((.*?)\\)$");
		Pattern setPattern = Pattern.compile("^set\\s*\\((.*?)\\)$");
		Pattern ifPattern = Pattern.compile("^if\\s*\\((.*?)\\)\\s*\\{(.*?)\\}$");
		Pattern whilePattern = Pattern.compile("^while\\s*\\((.*?)\\)\\s*\\{(.*?)\\}$");
		Pattern forPattern = Pattern.compile("^for\\s*\\((.*?)\\)\\s*\\{(.*?)\\}$");

		Matcher echoMatch = echoPattern.matcher(statement.getStatement());
		Matcher setMatch = setPattern.matcher(statement.getStatement());
		Matcher ifMatch = ifPattern.matcher(statement.getStatement());
		Matcher whileMatch = ifPattern.matcher(statement.getStatement());
		Matcher forMatch = ifPattern.matcher(statement.getStatement());

		if (echoMatch.find()) {
			statement.setType(StatementType.Echo);
			String s = echoMatch.group(1);
			Statement sub = new Statement();
			sub.setStatement(s);
			ArrayList<Statement> r = new ArrayList<Statement>();
			r.add(sub);
			r.add(statement);
			return r;
		} else if (setMatch.find()) {
			statement.setType(StatementType.Set);
			String s = setMatch.group(1);
			Statement sub = new Statement();
			sub.setStatement(s);
			ArrayList<Statement> r = new ArrayList<Statement>();
			r.add(sub);
			r.add(statement);
			return r;
		} else if (ifMatch.find()) {
			statement.setType(StatementType.If);
			System.out.println(ifMatch.group(1) + " ," + ifMatch.group(2));
			String s = ifMatch.group(1);
			Statement sub = new Statement();
			sub.setStatement(s);
			ArrayList<Statement> r = new ArrayList<Statement>();
			r.add(sub);
			r.add(statement);
			return r;
		} else {
			statement.setType(StatementType.Exp);
			ArrayList<Statement> r = new ArrayList<Statement>();
			r.add(statement);
			return r;
		}
	}
}