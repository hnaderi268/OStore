package Interpreter;
 
import java.util.ArrayList;
import java.util.Stack;

import Model.Process;
import Model.Statement;

public class Interpreter {
	private Process runningPorcess =null;
	private long intruptTime;
	private int statementRunLimit = -1;
	private boolean useTimer = false;
	private boolean limitedStatement = false;
	private Compiler compiler;
	private Assembler assembler;
	
	private Stack<String> returnStack;
	
	public Interpreter(){
		compiler = new Compiler();
		assembler = new Assembler();
		returnStack = new Stack<String>();
	}
	
	public void startRunning() throws Exception{
		if(runningPorcess != null){
			if(!useTimer){
				if(!limitedStatement){
					while(runningPorcess.hasNext()){
						Statement statement = runningPorcess.getNext();
						if(statement.isAssembly()){
							assembler.assembleAndRun(runningPorcess,statement,returnStack);
						}else{
							ArrayList<Statement> r =compiler.compile(statement);
							runningPorcess.replaceStatement(r);
						}
					}
					flush();
				}
			}
		}
	}
	
	public void flush(){
		runningPorcess.flush();
	}
	
	public void setRunningProcess(Process p){
		runningPorcess = p;
	}
	
	public void resetInterpreer(){
		runningPorcess = null;
		useTimer = false;
		limitedStatement = false;
	}
}
