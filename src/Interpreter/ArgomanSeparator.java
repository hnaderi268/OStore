package Interpreter;

import java.util.ArrayList;

//این کلاس برای اینه که  وقتیکقادیر ورودی یک تابع رو گرفتیم بتونیم از هم جداشون کنیم
//یه رشته ورودی داریم با یه کاراکتر که بین آرگومان ها قرار می گیره. در یک اری لیست هم به ترتیی آرگومان
//ها در خروجی قرار می گیرند.
public class ArgomanSeparator {

	public ArrayList<String> separate(String input, char separator) throws Exception {
		
		input += separator;
		Exception standardInput = null;
		ArrayList argoms = new ArrayList();
		String currentArgom = "";
		int parsOpened = 0; // parantheses opened till now

		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) == '(')
				parsOpened++;
			else if (input.charAt(i) == ')')
				parsOpened--;
			
			if (parsOpened < 0)
				throw standardInput;
			else if (input.charAt(i) == separator && parsOpened == 0) {
				argoms.add(currentArgom);
				currentArgom = "";
			} else {
				currentArgom += input.charAt(i);
			}
		}
		if(parsOpened!=0)
			throw standardInput;
		return argoms;
	}
}
