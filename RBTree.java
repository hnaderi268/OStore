import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Naderi
 */
public class RBTree {
	static Scanner sc;
	static RBTree tree;

	public static void main(String[] args) {
		final long startTime = System.currentTimeMillis();

		tree = new RBTree();
		sc = new Scanner(System.in);
//		 while (sc.hasNext()) {
//		 parse(sc.next());
//		 }
//		for (int i = 0; i < 5000; i++) {
//			if (i % 10 == 0){
//				tree.delete((int) (Math.random() * 1000));}
//			else if (i % 25 == 0){
//				tree.print();}
//			else
//				tree.insert((int) (Math.random() * 1000));
		
//		}
		ArrayList arr=new ArrayList();
		for(int i=0;i<10000;i++)
			arr.add((int)(Math.random()*100000));
			arr.sort(null);
			System.out.println(arr);
			final long endTime = System.currentTimeMillis();
			System.out.println((endTime - startTime));	
		// tree.insert(10);
		// tree.insert(30);
		// tree.insert(50);
		// tree.insert(70);
		// tree.insert(90);
		// tree.insert(110);
		// tree.insert(130);
		// tree.insert(20);
		// tree.insert(100);
		// tree.printIN();
		// System.out.println("OOOOOSXSXSX");
		// tree.delete(50);
		// //// tree.insert(100);
		// //// tree.insert(2);
		// //// tree.insert(13);
		// //// tree.insert(6);
		// //
		// tree.printIN();
	}

	public static void parse(String cmd) {
		if (cmd.charAt(0) == 'i') {
			int n = sc.nextInt();
			tree.insert(n);
		} else if (cmd.charAt(0) == 'd') {
			int n = sc.nextInt();
			tree.delete(n);
		} else {
			tree.print();
		}
	}

	private void print() {
		RBNode[] list = new RBNode[10000];
		int size = 0;
		list[size++] = this.root;
		for (int i = 0; i < size; i++) {
			// if(list[i].parent!=null)
			// //System.out.print(list[i].parent.value+" ");
			if (list[i].left != null && list[i].left.value != -1)
				list[size++] = list[i].left;
			if (list[i].right != null && list[i].right.value != -1)
				list[size++] = list[i].right;
			System.out.print(list[i].value);
			if (list[i].isRed)
				System.out.print("r");
			else
				System.out.print("b");
			if (i < size - 1)
				System.out.print(" ");

		}
		System.out.println();
	}

	class RBNode {

		boolean isRed;// 0 is black
		int value;
		RBNode left, right;
		RBNode parent;

		public RBNode(int x) {
			isRed = true;
			value = x;
			parent = null;
		}

	}

	RBNode root;

	public RBTree() {

	}

	public void printIN() {
		// System.out.println();
		// System.out.println();
		printIN(root);
	}

	private void printIN(RBNode t) {
		if (t == null) {
			return;
		}

		if (t.value != -1) {
			System.out.println("val: " + t.value + " col: " + t.isRed);
			if (t.left != null) {
				System.out.println("LEFTval: " + t.left.value + " col: " + t.left.isRed);
			}
			if (t.left != null) {
				System.out.println("RiGHTTval: " + t.right.value + " col: " + t.right.isRed);
			}
			if (t.parent != null) {
				System.out.println("PAREnetval: " + t.parent.value + " col: " + t.parent.isRed);
			}
			System.out.println();
		}
		printIN(t.left);
		printIN(t.right);
	}

	public void insert(int x) {
		RBNode t = new RBNode(x);
		t.left = new RBNode(-1);
		t.left.isRed = false;
		t.left.parent = t;
		t.right = new RBNode(-1);
		t.right.isRed = false;
		t.right.parent = t;
		if (root == null) {
			root = t;
			t.isRed = false;
		} else {
			insert(root, t);
			insertBalance1(t);
		}

	}

	public RBNode gParent(RBNode x) {
		if (x.parent != null) {
			return x.parent.parent;
		}
		return null;
	}

	public RBNode uncle(RBNode x) {
		RBNode t = gParent(x);
		if (t != null) {
			if (t.left == x.parent) {
				return t.right;
			} else {
				return t.left;
			}
		}
		return null;
	}

	public RBNode insert(RBNode t, RBNode key) {
		if (t == null || t.value == -1) {
			t = key;

			if (t.parent != null) {
				// System.out.println(t.value + " par: " + t.parent.value);

			}
			if (gParent(t) != null) {
				// System.out.println(t.value + " Gpar: " + gParent(t).value);

			}
			if (uncle(t) != null) {
				// System.out.println(t.value + " uncle: " + uncle(t).value);

			}

		}

		if (key.value < t.value) {
			key.parent = t;
			// System.out.println(t.value);
			t.left = insert(t.left, key);

		}
		if (key.value > t.value) {
			key.parent = t;
			// System.out.println(key.value);
			// System.out.println("mn" + t.value);
			t.right = insert(t.right, key);

		}
		return t;
	}

	private void insertBalance1(RBNode t) {
		if (t == root) {
			// System.out.println("FUlO");
		}
		if (t.parent == null || t == root) {
			t.isRed = false;
		} else {
			// System.out.println(1);
			insertBalance2(t);
		}
	}

	private void insertBalance2(RBNode t) {
		if (t.parent.isRed) {
			// System.out.println(2);
			insertBalance3(t);
		}
	}

	private void insertBalance3(RBNode t) {
		// System.out.println("boghz");
		// System.out.println("bal3:");
		// System.out.println(t.value);
		// //System.out.println(gParent(t).value);
		// System.out.println(uncle(t).value);
		// System.out.println(3);
		if (uncle(t).isRed && t.parent.isRed) {
			// System.out.println("");
			uncle(t).isRed = false;
			t.parent.isRed = false;
			gParent(t).isRed = true;
			// System.out.println("OPOPOPOP" + t.parent.value);
			// System.out.println("OPOPOPOP" + t.value);
			insertBalance1(gParent(t));
		} else {
			insertBalance4(t);
		}
	}

	public void LRotate(RBNode t) {
		RBNode p = t.right;
		RBNode q = p.left;
		t.right = q;
		p.left = t;
		p.parent = t.parent;
		t.parent = p;
		q.parent = t;
		if (p.parent != null) {
			if (t == p.parent.left) {
				p.parent.left = p;

			} else {
				p.parent.right = p;
			}
		} else {
			root = p;

		}
	}

	public void RRotate(RBNode t) {
		RBNode p = t.left;
		RBNode q = p.right;
		t.left = q;
		p.right = t;
		p.parent = t.parent;
		t.parent = p;
		if (q != null)
			q.parent = t;
		if (p.parent != null) {
			if (t == p.parent.left) {
				p.parent.left = p;

			} else {
				p.parent.right = p;
			}
		} else {
			root = p;

		}
	}

	private void insertBalance4(RBNode t) {
		// System.out.println("ghuchi");
		// System.out.println(4);
		if (t.parent.right == t && gParent(t).left == t.parent) {
			LRotate(t.parent);
			insertBalance5(t.left);
		} else if (t.parent.left == t && gParent(t).right == t.parent) {
			RRotate(t.parent);
			insertBalance5(t.right);
		} else {
			insertBalance5(t);
		}
	}

	private void insertBalance5(RBNode t) {
		// System.out.println(5);
		t.parent.isRed = false;
		gParent(t).isRed = true;
		// System.out.println("gh");
		// System.out.println(gParent(t).isRed + " " + gParent(t).value);
		// printIN(root);
		// System.out.println("OOOOO");
		// System.out.println(t.parent.value);
		// System.out.println(t.parent.left.value);
		// System.out.println(t.parent.right.value);
		// System.out.println(t.value);
		if (t == t.parent.left) {
			// System.out.println("PPPPPPPPPPPPp");
			RRotate(gParent(t));
		} else {
			LRotate(gParent(t));
		}
	}

	public void delete(int x) {
		delete(root, x);
	}

	public void delete(RBNode t, int key) {
		if (t == null || t.value == -1) {
			return;
		}
		if (t.value == key) {
			if (t.left.value != -1 && t.right.value != -1) {
				RBNode minn = t.right;
				for (; minn.left.value != -1; minn = minn.left)
					;
				t.value = minn.value;
				delete_one(minn);
			} else {
				delete_one(t);
			}
		} else if (t.value < key) {
			delete(t.right, key);
		} else {
			delete(t.left, key);
		}

	}

	public RBNode sibling(RBNode t) {
		if (t.parent == null || t == null) {
			return null;
		}
		if (t == t.parent.left)
			return t.parent.right;
		else
			return t.parent.left;
	}

	public void delete_one(RBNode t) {
		RBNode child;
		if (t.left.value != -1) {
			child = t.left;
		} else {
			child = t.right;
		}
		// System.out.println("hj"+t.value+" "+child.value);
		child.parent = t.parent;
		if (t.parent != null) {
			if (t.parent.left == t) {
				t.parent.left = child;

			} else {
				// System.out.println("PPPPiiiiP"+t.parent.right.value);System.out.println("PPPPiiiiP"+t.parent.left.value);
				t.parent.right = child;
			}
		} else {
			root = child;
		}
		if (!t.isRed) {
			if (child.isRed) {
				child.isRed = false;
			} else {
				deleteCase1(child);
			}
		}
		return;
	}

	public void deleteCase1(RBNode t) {
		if (t.parent == null || root == t) {

		} else {
			deleteCase2(t);
		}
	}

	public void deleteCase2(RBNode t) {
		if (sibling(t).isRed) {
			if (t.parent.left == t) {
				t.parent.isRed = true;
				sibling(t).isRed = false;
				LRotate(t.parent);

			} else {
				t.parent.isRed = true;
				sibling(t).isRed = false;
				RRotate(t.parent);
			}
		}
		deleteCase3(t);
	}

	public void deleteCase3(RBNode t) {
		RBNode s = sibling(t);
		if (!t.parent.isRed && !s.isRed && !s.left.isRed && !s.right.isRed) {
			s.isRed = true;
			deleteCase1(t.parent);
		} else {
			deleteCase4(t);
		}
	}

	public void deleteCase4(RBNode t) {
		RBNode s = sibling(t);
		if (t.parent.isRed && !s.isRed && !s.left.isRed && !s.right.isRed) {
			s.isRed = true;
			t.parent.isRed = false;
		} else {
			deleteCase5(t);
		}
	}

	public void deleteCase5(RBNode t) {
		RBNode s = sibling(t);
		if (!s.isRed) {
			if (t == t.parent.left && !s.right.isRed && s.left.isRed) {
				s.isRed = true;
				s.left.isRed = false;
				RRotate(s);
			} else if (t == t.parent.right && s.right.isRed && !s.left.isRed) {
				s.isRed = true;
				s.right.isRed = false;
				LRotate(s);

			}
		}
		deleteCase6(t);
	}

	public void deleteCase6(RBNode t) {
		RBNode s = sibling(t);
		s.isRed = t.parent.isRed;
		t.parent.isRed = false;
		if (t == t.parent.left) {
			s.right.isRed = false;
			LRotate(t.parent);
		} else {
			// printIN();
			// System.out.println("PPPPOPOPO"+t.value);
			// System.out.println("PPPPOPOPO"+s.value);
			// System.out.println("PPPPOPOPO"+t.parent.value);
			// System.out.println("PPPPOPOPO"+t.parent.left.value);
			s.left.isRed = false;
			RRotate(t.parent);
		}
	}

}
